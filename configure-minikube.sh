#!/bin/bash
set -e

stop_script() {
    echo 'Stopping the script...'
    exit 1
}

if ! minikube version; then
    echo 'This script requires Minikube to be installed.' >&2
    stop_script
fi

if ! helm > /dev/null; then
    echo 'This script requires Helm to be installed.' >&2
    stop_script
fi

if minikube status | grep -iq "minikube: [Running|Stopped]"; then
    echo 'Minikube must be stopped and deleted before running this script.'
    echo 'Note that this will destroy current volume data.'
    printf 'Would you like me to do this for you? [y/n] '
    read should_stop

    if [[ $should_stop == "y" ]]; then
        if ! minikube stop && minikube delete; then
            echo 'Looks like there was a problem. Use that smart brain of yours to figure it out.' >&2
            stop_script
        fi
    else
        stop_script
    fi
fi

echo 'Define your resource requirements:'

DEFAULT_MINIKUBE_MEMORY=24576
printf 'Memory (Default: %s): ' $DEFAULT_MINIKUBE_MEMORY
read MINIKUBE_MEMORY
MINIKUBE_MEMORY="${MINIKUBE_MEMORY:=$DEFAULT_MINIKUBE_MEMORY}"

DEFAULT_MINIKUBE_CPU=12
printf 'CPU Cores (Default: %s): ' $DEFAULT_MINIKUBE_CPU
read MINIKUBE_CPU
MINIKUBE_CPU="${MINIKUBE_CPU:=$DEFAULT_MINIKUBE_CPU}"

DEFAULT_MINIKUBE_STORAGE=80g
printf 'Disk Space (Default: %s): ' $DEFAULT_MINIKUBE_STORAGE
read MINIKUBE_STORAGE
MINIKUBE_STORAGE="${MINIKUBE_STORAGE:=$DEFAULT_MINIKUBE_STORAGE}"

echo ''

if ! minikube start --memory $MINIKUBE_MEMORY --cpus $MINIKUBE_CPU --disk-size $MINIKUBE_STORAGE; then
    echo "There was a problem starting the cluster. Deep breaths. You'll figure it out." >&2
    stop_script
fi

minikube addons disable default-storageclass || true
minikube addons enable metrics-server || true

if ! minikube ssh "sudo sysctl -w vm.max_map_count=262144"; then
    echo "I couldn't configure Minikube to support Elasticsearch, which is super necessary." >&2
    echo "Look into setting the VM's vm.max_map_count manually." >&2
    stop_script
fi

if ! helm init > /dev/null; then
    echo "Helm couldn't be initialized. Go ahead and show Google search what you're made of." >&2
    stop_script
fi

if ! helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator; then
    echo "The incubator repo for Kafka couldn't be added. It's sort of a requirement :/"
    stop_script
fi

cd botbin
if ! helm dependency update; then
    echo "Helm couldn't fetch all required charts. We kind of need them..."
    cd ..
    stop_script
fi
cd ..

if ! minikube addons enable ingress; then
    echo 'There was a problem installing some networking components.'
    echo 'Action Plan:'
    echo 'First Best Solution: Try enabling the Minikube ingress addon manually.'
    echo 'First Worst Solution: Turn off this feature in the chart values file.'
    stop_script
fi

echo 'I can edit your hosts file so that you can access frontend services through URLs such as dev.botbin.io'
printf 'Is that okay? [y/n] '
read should_edit_hosts

if [[ $should_edit_hosts == 'y' ]]; then
    echo "$(minikube ip) api.dev.botbin.io docs.dev.botbin.io river.dev.botbin.io dev.botbin.io kibana.dev.botbin.io" \
        | sudo tee -a /etc/hosts
fi

printf "\nHey. We did it. Go us :D\n"