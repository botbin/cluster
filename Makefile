all:
	helm upgrade --install botbin botbin

clean:
	helm del --purge botbin || true
