# cluster
This project contains the global configuration for BotBin. It should allow
anyone with the proper access level to deploy all components of the service
into a Kubernetes cluster.

## Contents
- [System Diagram](#system-diagram)
- [Development](#development)
    - [Prerequisites](#prerequisites)
    - [Local Environment](#local-environment)
    - [Deployment](#deployment)
- [Navigating the Cluster](#navigating-the-cluster)
    - [Ingress](#ingress)
    - [Labels](#labels)

## System Diagram
![System Diagram](system.png)

## Development
Development will target [Minikube](https://github.com/kubernetes/minikube)
until the minimum viable product is complete.
Manifest templating ensures that minimal changes will be required to shift into
production mode with [kops](https://github.com/kubernetes/kops) and AWS when the
time comes.

### Prerequisites
With the help of [Kubernetes](https://kubernetes.io/) and [Helm](https://helm.sh/),
we can deploy or tear down all system services using one command. But
before reading further, you should start the process of acquiring a JSON
key, which is needed to pull images from the Docker registry.

### Local Environment
Local development will require these two tools:
- Minikube
    - Allows you to virtualize a cluster on a single node. Installation
      instructions are here: https://kubernetes.io/docs/tasks/tools/install-minikube/.
      You should use VirtualBox as your hypervisor, which may require the
      installation of additional packages.
- Helm
    - Enables deployment of all resources as a single entity. Installation
      instructions are here: https://docs.helm.sh/using_helm/#installing-helm.

Once they are installed, run the `configure-minikube.sh` script.

### Deployment
#### Secrets
The Helm chart can deploy a handful of dummy secrets needed for the services
to operate. However, certain secrets are based on files that you must acquire
and add to the botbin/files/secrets/ directory. A project maintainer can
assist you in getting these assets.

#### make
So you have Minikube, Helm, and your secrets. This reduces operations to the
following two commands:  
- `make`
    - This will deploy BotBin into your cluster.
- `make clean`
    - This will reverse all changes made by running `make`.

## Navigating the Cluster
### Ingress
If you follow the Minikube config script to the end, your host and cluster
will provide simplified endpoints for frontend services. They are:
- dev.botbin.io
    - The web client that users will interact with
- kibana.dev.botbin.io
    - A Kibana dashboard for exploring application logs
- api.dev.botbin.io
    - The API gateway
- docs.dev.botbin.io
    - Documentation for the REST APIs
- river.dev.botbin.io
    - The river WebSocket server
    
### Labels
Currently, all services and deployments other than the OpenFaaS resources have
`tier` labels. The standard values are
- `frontend`
    - Applications that are directly accessible outside of the cluster. Any application
      that does not fit this tier is assumed to only be accessible within the cluster.
- `app`
    - Applications that focus on problem domains specific to this project
- `message`
    - Applications that either are or behave similarly to message brokers
- `data`
    - Applications that store data in a durable, medium-to-long term way