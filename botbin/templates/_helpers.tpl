{{/*
    Generates a username:password string for use with Google Container Registry.
    This template requires a JSON key file with storage roles to be in the
    "<chart_root>/files/secrets/" directory with the name "gcr-key.json".
*/}}
{{ define "GCR_REG_LOGIN" }}_json_key:{{ .Files.Get "files/secrets/gcr-key.json" }}{{ end }}

{{/*
    Generates a dockerconfig JSON object for use with Google Container Registry.
    By base64-encoding this template output, you will possess a valid string to
    give the .dockerconfigjson data field of a kubernetes.io/dockerconfigjson secret.
    That secret allows Kubernetes objects to pull images from the private registry.
*/}}
{{- define "GCR_DOCKER_CONFIG" }}
{
    "auths": {
        "gcr.io": {
            "auth": "{{ include "GCR_REG_LOGIN" . | b64enc }}"
        }
    }
}
{{- end }}